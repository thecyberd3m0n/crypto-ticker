describe('Binance Widget', () => {
    beforeEach(() => {
        cy.server();
        cy
            .route('GET',"/**")
            .as('getMarkets');
    })
    it('Gets the markets from Binance and shows the table', () => {
        cy.visitWithDelWinFetch('/');

        cy.wait('@getMarkets').then(() => {
            cy.get('div.ReactVirtualized__Grid.ReactVirtualized__Table__Grid').find(' > div').its('length').should('be.gte', 0)
        })

    });
});
