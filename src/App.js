import React, { useEffect, useState, useRef } from 'react';
import { TopBar } from './components/TopBar/TopBar';
import { Footer } from './components/Footer/Footer';
import { MainList } from './components/MainList/MainList';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './consts/theme';
import 'App.css';

const MARKETS_ENDPOINT = '/exchange-api/v1/public/asset-service/product/get-products';
const UPDATES_ENDPOINT = 'wss://stream.binance.com/stream?streams=!miniTicker@arr';

export function App() {
    const [ markets, setMarkets ] = useState([]);
    const [ selectedMarket, setSelectedMarket ] = useState(0);
    const [ selectedCategory, setSelectedCategory ] = useState('ALL');
    const [ categories, setCategories ] = useState({});
    const [ searchBar, setSearchBar ] = useState('');
    const [ isPaused, setPause ] = useState(false);
    const ws = useRef(null);

    useEffect(() => {
        async function fetchData() {
            // You can await here
            const response = await fetch(
                MARKETS_ENDPOINT
            ).then((response) => response.json());
            setMarkets(
                response.data.map((market) => {
                    market.latest = market.c;
                    return market;
                })
            );
            let categories = {};
            response.data.forEach((market) => {
                if (!categories[market.pn]) {
                    categories[market.pn] = [ market.q ];
                } else if (!categories[market.pn].includes(market.q)) {
                    categories[market.pn].push(market.q);
                }
            });
            setCategories(categories);
        }
        fetchData();
    }, []);

    useEffect(() => {
        ws.current = new WebSocket(UPDATES_ENDPOINT);
        ws.current.onopen = () => console.log('ws opened');
        ws.current.onclose = () => console.log('ws closed');

        return () => {
            ws.current.close();
        };
    }, []);

    useEffect(
        () => {
            if (!ws.current) return;

            ws.current.onmessage = (e) => {
                if (isPaused || !markets) return;
                const message = JSON.parse(e.data);
                const newMarkets = markets.map((market) => {
                    const foundMarket = message.data.find((element) => element.s === market.s);
                    if (foundMarket) {
                        return {
                            ...market,
                            ...{
                                h: parseFloat(foundMarket.h),
                                o: parseFloat(foundMarket.o),
                                v: parseInt(foundMarket.v),
                                latest: parseFloat(foundMarket.c),
                                l: parseFloat(foundMarket.l),
                                rises: parseFloat(foundMarket === market.c)
                                    ? -1
                                    : parseFloat(foundMarket.c) < parseFloat(market.latest)
                                      ? 1
                                      : parseFloat(foundMarket.c) > parseFloat(market.latest) ? 0 : null
                            }
                        };
                    } else {
                        return market;
                    }
                });
                setMarkets(newMarkets);
            };
        },
        [ isPaused, markets ]
    );

    const handleMarketChange = (market) => {
        setSelectedMarket(market);
        setSelectedCategory('ALL');
    };

    const handleCategoryChange = (category) => {
        if (categories[Object.keys(categories)[selectedMarket]].length > 1) {
            setSelectedCategory(category);
        }
    };

    const onTogglePause = () => {
        setPause(!isPaused);
    };
    return (
        <MuiThemeProvider theme={theme}>
            <TopBar
                onSearchFieldChange={(textField) => {
                    setSearchBar(textField);
                }}
                selectedMarket={selectedMarket}
                selectedCategory={selectedCategory}
                onCategoryChange={(e) => handleCategoryChange(e)}
                onMarketChange={(e) => handleMarketChange(e)}
                categories={categories}
                onPause={onTogglePause}
                isPaused={isPaused}
            />
            <MainList
                markets={markets}
                filter={searchBar}
                categories={categories}
                selectedMarket={Object.keys(categories)[selectedMarket]}
                selectedCategory={selectedCategory}
            />
            <Footer />
        </MuiThemeProvider>
    );
}
