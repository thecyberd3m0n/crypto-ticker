import PropTypes from "prop-types";
import React from 'react';
import { VirtualizedTable } from 'components/VirtualizedTable/VirtualizedTable';
import Paper from '@material-ui/core/Paper';

export function MainList(props) {
    const { markets, filter, selectedMarket, selectedCategory } = props;

    const [ order, setOrder ] = React.useState('asc');
    const [ orderBy, setOrderBy ] = React.useState('pair');

    const descendingComparator = (a, b, orderBy) => {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const getComparator = (order, orderBy) => {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    };

    const stableSort = (array, comparator) => {
        const stabilizedThis = array.map((el, index) => [ el, index ]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    };

    let filteredMarkets = markets
        ? markets
              .filter((element) => {
                  return filter.length > 0
                      ? element.q.toLowerCase() === filter.toLowerCase() ||
                        element.b.toLowerCase() === filter.toLowerCase()
                      : true;
              })
              .filter((element) => {
                  return selectedCategory === 'ALL' ? element.pm === selectedMarket : element.q === selectedCategory;
              })
              .map((element) => {
                  element.pair = `${element.b} / ${element.q}`;
                  return element;
              })
        : [];

    filteredMarkets = stableSort(filteredMarkets, getComparator(order, orderBy));
    return (
        <Paper style={{ height: `calc(100vh - 128px)`, width: '100%' }}>
            <VirtualizedTable
                rowCount={filteredMarkets.length}
                rowGetter={({ index }) => filteredMarkets[index]}
                onRequestSort={(event, property) => handleRequestSort(event, property)}
                order={order}
                orderBy={orderBy}
                columns={[
                    {
                        width: 300,
                        label: 'Pair',
                        dataKey: 'pair'
                    },
                    {
                        width: window.innerWidth / 4,
                        label: 'High',
                        dataKey: 'h',
                        numeric: true
                    },
                    {
                        width: window.innerWidth / 4,
                        label: 'Low',
                        dataKey: 'l',
                        numeric: true
                    },
                    {
                        width: window.innerWidth / 4,
                        label: 'Open',
                        dataKey: 'o',
                        numeric: true
                    },
                    {
                        width: window.innerWidth / 4,
                        label: 'Latest',
                        dataKey: 'latest',
                        numeric: true
                    },
                    {
                        width: window.innerWidth / 4,
                        label: 'Volume',
                        dataKey: 'v',
                        numeric: true
                    }
                ]}
            />
        </Paper>
    );
}

MainList.propTypes = {
  filter: PropTypes.string,
  markets: PropTypes.array,
  selectedCategory: PropTypes.string,
  selectedMarket: PropTypes.string,
}
