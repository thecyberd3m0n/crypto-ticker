import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import logo from './arcsoftware-opacle.png';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  text: {
    padding: theme.spacing(2, 2, 0),
  },
  paper: {
    paddingBottom: 50,
  },
  list: {
    marginBottom: theme.spacing(2),
  },
  subheader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    top: 'auto',
    bottom: 0,
    minHeight: '60px',
    alignItems: 'end',
    justifyContent: 'center'
  },
  creatorIcon: {
    width: '200px',
    marginLeft: '1em'
  }
}));

export function Footer() {
  const classes = useStyles();

  return (
    <AppBar position="static" color="primary" className={classes.appBar}>
      <Grid container justify="center">
        <Grid item xs={6} md={2} lg={2}>
          <Typography variant="caption">
            Developped by:
          </Typography>
          <img
            src={logo}
            alt="Creator"
            className={classes.creatorIcon}
          />
        </Grid>
      </Grid>
    </AppBar>
  );
}