import PropTypes from "prop-types";
import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Stop from '@material-ui/icons/Stop';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import bitcoin from './Bitcoin.svg';

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'block'
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    width: '100%'
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  selectFormControlRoot: {
    width:'2em',
    minWidth: '2em'
  },
  inputInput: {
    width: '100%',
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  tab: {
    width: 40
  },
  icon: {
    float: 'right'
  }
}));

export function TopBar(props) {
  const classes = useStyles();
  const {
    onSearchFieldChange,
    onMarketChange,
    categories,
    selectedMarket,
    onPause,
    isPaused,
    onCategoryChange,
    selectedCategory
  } = props;
  const displayCategorySelect = categories[Object.keys(categories)[selectedMarket]] &&
    categories[Object.keys(categories)[selectedMarket]].length > 1;
  return (
    <div className={classes.grow}>
      <AppBar position="sticky">
        <Toolbar>
          <Grid container spacing={2} justify="space-around"
            alignItems="center" wrap={'nowrap'}>
            <Grid item xs={1} lg={1}>
              <img width="48px" height="48px" src={bitcoin} alt="Bitcoin" />
            </Grid>
            <Grid item xs={!displayCategorySelect ? 10 : 6} lg={!displayCategorySelect ? 6 : 5}>
              <Tabs
                value={selectedMarket}
                className={classes.tabs}
                variant="scrollable"
                onChange={(event, newValue) => {
                  onMarketChange(newValue);
                }}
                aria-label="markets"
              >
                {Object.keys(categories).map((item, index) => {
                    return <Tab key={item} value={index} label={item} />;
                })}
              </Tabs>
            </Grid>

              <Grid item xs={4} lg={!displayCategorySelect ? 1 : 2}>
                {displayCategorySelect && (
                  <FormControl className={{
                      root: classes.selectFormControlRoot
                    }}>
                    <Select
                      autoWidth={true}
                      value={selectedCategory}
                      onChange={(e) => onCategoryChange(e.target.value)}
                      inputProps={{
                        name: 'subcategory',
                        id: 'subcategory-native-label-placeholder'
                      }}
                    >
                      <MenuItem defaultValue value={'ALL'}>
                        ALL
                        </MenuItem>
                      {categories[Object.keys(categories)[selectedMarket]].map((subcategory) => (
                        <MenuItem key={subcategory} value={subcategory}>{subcategory}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              </Grid>
            <Hidden only={['xs', 'sm', 'md']}>
              <Grid item lg={displayCategorySelect ? 5 : 6}>
                <div className={classes.search}>
                  <div className={classes.searchIcon}>
                    <SearchIcon />
                  </div>

                  <InputBase
                    placeholder="Search market…"
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput
                    }}
                    inputProps={{ 'aria-label': 'search' }}
                    onChange={(e) => onSearchFieldChange(e.target.value)}
                  />
                </div>
              </Grid>
            </Hidden>
            <Hidden only={['xs', 'sm', 'md']}>
              <Grid item lg={1}>
                <div className={classes.icon}>
                <IconButton onClick={onPause} color="inherit">
                  {isPaused ? <PlayArrow /> : <Stop />}
                </IconButton>
                </div>
              </Grid>
            </Hidden>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

TopBar.propTypes = {
  categories: PropTypes.object,
  isPaused: PropTypes.bool.isRequired,
  onCategoryChange: PropTypes.func,
  onMarketChange: PropTypes.func,
  onPause: PropTypes.func,
  onSearchFieldChange: PropTypes.func,
  selectedCategory: PropTypes.string,
  selectedMarket: PropTypes.number
}
