import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';

import { AutoSizer, Column, Table } from 'react-virtualized';

const styles = (theme) => ({
    flexContainer: {
        display: 'flex',
        alignItems: 'center',
        boxSizing: 'border-box'
    },
    table: {
        // temporary right-to-left patch, waiting for
        // https://github.com/bvaughn/react-virtualized/issues/454
        '& .ReactVirtualized__Table__headerRow': {
            flip: false,
            paddingRight: theme.direction === 'rtl' ? '0 !important' : undefined
        }
    },
    tableRow: {
        cursor: 'pointer'
    },
    tableRowHover: {
        '&:hover': {
            backgroundColor: theme.palette.grey[200]
        }
    },
    tableCell: {
        flex: 1
    },
    noClick: {
        cursor: 'initial'
    },
    green: {
      color: green[600]
    },
    red: {
      color: red[600]
    }
});

function MuiVirtualizedTable(props) {
    const { classes, columns, rowHeight, headerHeight, ...tableProps } = props;

    const getRowClassName = ({ index }) => {
        const { classes, onRowClick } = props;

        return clsx(classes.tableRow, classes.flexContainer, {
            [classes.tableRowHover]: index !== -1 && onRowClick != null
        });
    };

    const cellRenderer = ({ cellData, columnIndex, rowData }) => {
        const { columns, classes, rowHeight, onRowClick } = props;
        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, {
                    [classes.noClick]: onRowClick == null,
                    [classes.red]: rowData.rises === 0,
                    [classes.green]: rowData.rises === 1
                })}
                variant="body"
                style={{ height: rowHeight }}
                align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
            >
                {cellData}
            </TableCell>
        );
    };

    const headerRenderer = ({ label, columnIndex }) => {
        const { headerHeight, columns, classes, order, onRequestSort, orderBy } = props;
        const createSortHandler = (property) => (event) => {
            onRequestSort(event, property);
        };
        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, classes.noClick)}
                variant="head"
                style={{ height: headerHeight }}
                sortDirection={orderBy === columns[columnIndex].dataKey ? order : false}
                align={columns[columnIndex].numeric || false ? 'right' : 'left'}
            >
                <TableSortLabel
                    active={orderBy === columns[columnIndex].dataKey}
                    direction={orderBy === columns[columnIndex].dataKey ? order : 'asc'}
                    onClick={createSortHandler(columns[columnIndex].dataKey)}
                >
                    {<span>{label}</span>}
                </TableSortLabel>
            </TableCell>
        );
    };

    return (
        <AutoSizer>
            {({ height, width }) => (
                <Table
                    height={height}
                    width={width}
                    rowHeight={rowHeight}
                    gridStyle={{
                        direction: 'inherit'
                    }}
                    headerHeight={headerHeight}
                    className={classes.table}
                    {...tableProps}
                    rowClassName={getRowClassName}
                >
                    {columns.map(({ dataKey, ...other }, index) => {
                        return (
                            <Column
                                key={dataKey}
                                headerRenderer={(headerProps) =>
                                    headerRenderer({
                                        ...headerProps,
                                        columnIndex: index
                                    })}
                                className={classes.flexContainer}
                                cellRenderer={cellRenderer}
                                dataKey={dataKey}
                                {...other}
                            />
                        );
                    })}
                </Table>
            )}
        </AutoSizer>
    );
}

MuiVirtualizedTable.propTypes = {
    classes: PropTypes.object.isRequired,
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            dataKey: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
            numeric: PropTypes.bool,
            width: PropTypes.number.isRequired
        })
    ).isRequired,
    headerHeight: PropTypes.number,
    onRowClick: PropTypes.func,
    rowHeight: PropTypes.number,
    onRequestSort: PropTypes.func,
    orderBy: PropTypes.string
};
MuiVirtualizedTable.defaultProps = {
    headerHeight: 48,
    rowHeight: 48
};

export const VirtualizedTable = withStyles(styles)(MuiVirtualizedTable);
