import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            light: '#f44336',
            main: '#0690AA',
            dark: '#1d1d1d',
            contrastText: '#fff',
        },
        secondary: {
            light: '#f44336',
            main: '#750A1E',
            dark: '#1d1d1d',
            contrastText: '#000',
        },
    },
});
export default theme;